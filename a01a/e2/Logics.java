package a01a.e2;

import java.util.List;

public interface Logics {
	
	/* Checks if the numbers are all equals
	 */
	boolean equalValues();
	
	/* Increment the relative button index
	 */
	void increment(Integer index);
	
	/* Get the element at index position 
	 */
	Integer getElementAt(Integer index);

	List<Integer> getValues();
}
