package a01a.e2;

import java.util.List;

public class LogicsImpl implements Logics{
	
	private List<Integer> values;
	
	public LogicsImpl(List<Integer> values) {
		super();
		this.values = values;
	}

	@Override
	public boolean equalValues() {
		return this.getValues().stream()
				.allMatch(v -> v.equals(this.getElementAt(1)) && 
						v != this.getValues().size());
	}

	@Override
	public void increment(Integer index) {
		this.values.set(index, this.values.get(index) + 1);
	}

	@Override
	public Integer getElementAt(Integer index) {
		return this.values.get(index);
	}

	@Override
	public List<Integer> getValues() {
		return this.values;
	}

}
