package a01a.e2;


import java.awt.FlowLayout;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.*;


public class GUI extends JFrame{
	
	public GUI(int size){
		this.setSize(500, 100);
		this.getContentPane().setLayout(new FlowLayout());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		Logics logics = new LogicsImpl(Stream.iterate(0, i -> i+1)
				.limit(size)
				.collect(Collectors.toList()));
		
		
		//Iterator<Integer> it = Stream.iterate(0, i -> i+1).iterator();
		
		Map<JButton, Integer> buttonsMap = new HashMap<>();
		for(Integer i = 0; i<size; i++) {
			JButton btn = new JButton(i.toString());
			this.getContentPane().add(btn);
			buttonsMap.put(btn, i);
		}
		buttonsMap.entrySet().forEach(b -> {
			b.getKey().addActionListener(e -> {
		
				logics.increment(b.getValue());
				b.getKey().setText(logics.getElementAt(b.getValue()).toString());
				b.getKey().setEnabled(!logics.getElementAt(b.getValue()).equals(size));

				if(logics.equalValues()) {
					System.exit(1);
				}
			});
				
		});
		
		JButton print = new JButton("Print");
		this.getContentPane().add(print);
		print.addActionListener(e -> {
			String str = "";
			Iterator<Integer> it = logics.getValues().iterator();
			while(it.hasNext()) {
				str = str.isEmpty() ? "<<" : str + "|";
				str = str + it.next();
			}
			System.out.println(str + ">>");
		});
		
		
		this.setVisible(true);
	}

}
