package a01a.e1;

import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class InfiniteSequenceOpsImpl implements InfiniteSequenceOps{
	
	@Override
	public <X> InfiniteSequence<X> ofValue(X x) {
		return () -> x;
	}

	@Override
	public <X> InfiniteSequence<X> ofValues(List<X> l) {

		return !l.isEmpty() ? new InfiniteSequence<>() {
			private Iterator<X> iterator = l.iterator();	
			@Override
			public X nextElement() {
				if(!this.iterator.hasNext()) {
					this.iterator = l.iterator();
				}
				return this.iterator.next();
			}
		} : null;
		
	}

	@Override
	public InfiniteSequence<Double> averageOnInterval(InfiniteSequence<Double> iseq, int intervalSize) {	
		// funziona la seconda volta che chiamo questo metodo?
		return () -> iseq.nextListOfElements(intervalSize)
				.stream()
				.mapToDouble(d -> d)
				.average()
				.getAsDouble();			
	}
	
	/*
	 *public List<Double> nLE(InfiniteSequence<Double> iseq, int intervalSize){
	 *	return iseq.nextListOfElements(intervalSize);
	 *}
	 */
	
	@Override
	public <X> InfiniteSequence<X> oneEachInterval(InfiniteSequence<X> iseq, int intervalSize) {
		return new InfiniteSequence<X>() {
			@Override
			public X nextElement() {
				List<X> list = iseq.nextListOfElements(intervalSize);
				return list.get(list.size());
			}
		};
	}

	@Override
	public <X> InfiniteSequence<Boolean> equalsTwoByTwo(InfiniteSequence<X> iseq) {
		return () -> iseq.nextElement().equals(iseq.nextElement());
	}

	@Override
	public <X, Y extends X> InfiniteSequence<Boolean> equalsOnEachElement(InfiniteSequence<X> isx,
			InfiniteSequence<Y> isy) {
		return () -> isx.nextElement().equals(isy.nextElement());
	}

	@Override
	public <X> Iterator<X> toIterator(InfiniteSequence<X> iseq) {
		return Stream.generate(() -> iseq.nextElement()).iterator();
	}

}
