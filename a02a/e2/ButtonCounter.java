package a02a.e2;

import javax.swing.JButton;

public class ButtonCounter extends JButton {
	
	private int counterX;
	private int counterY;
	private JButton button;
	
	public ButtonCounter(JButton button) {
		this.button = button;
		this.counterX = 0;
		this.counterY = 0;
	}
	
	public void checkAndIncrement(JButton button) {
		if (buttonsXEqual(button) && button.getText().equals("*")){
			this.counterX ++;
		}
		if(buttonsYEqual(button) && button.getText().equals("*")) {
			this.counterY ++;
		}
	}

	private boolean buttonsXEqual(JButton button) {
		return this.button.getX() == button.getX();
	}
	
	private boolean buttonsYEqual(JButton button) { 
		return this.button.getY() == button.getY(); 
	}

	public int getCounterX() {
		return this.counterX;
	}

	public void setCounterX(int counterX) {
		this.counterX = counterX;
	}

	public int getCounterY() {
		return this.counterY;
	}

	public void setCounterY(int counterY) {
		this.counterY = counterY;
	}

}
