package a02a.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    public GUI(int size) {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
        	final JButton source = (JButton)e.getSource();
            final ButtonCounter bt = new ButtonCounter(source);
            
            if(source.getText().equals(" ")) {
            	source.setText("*");
            }else {
            	source.setText(" ");
            }

            for(Component c : panel.getComponents()) {
            	if (c instanceof JButton) {
            		bt.checkAndIncrement((JButton)c);
            	}
            }
            if(bt.getCounterX() == Math.sqrt(panel.getComponentCount()) ||
            		bt.getCounterY() == Math.sqrt(panel.getComponentCount())) {
            	System.exit(0);
            }
        };
        
        for (int i=0;i<Math.pow(size, 2);i++){
            final JButton jb = new JButton(" ");
            jb.addActionListener(al);
            panel.add(jb);
        } 
        this.setVisible(true);

    }
    
}
