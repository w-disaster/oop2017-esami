package a02a.e1;

public class SequenceAcceptorImpl implements SequenceAcceptor{

	private Sequence sequence;
	private Pair<Integer, Integer> nextValues;
	
	
	@Override
	public void reset(Sequence sequence) {
		this.sequence = sequence;
		switch(sequence) { 
		case POWER2:
			this.nextValues = new Pair<>(1, 2);
			break;
		case FLIP:
			this.nextValues = new Pair<>(1, 0);
			break;
		case RAMBLE:
			this.nextValues = new Pair<>(0, 1);
			break;
		case FIBONACCI:
			this.nextValues = new Pair<>(1, 1);
			break;
		}
	}

	@Override
	public void reset() {
		if(!this.isEmpty()) {
			this.reset(this.sequence);
		} else {
			throw new IllegalStateException();
		}
	}

	private boolean isEmpty() {
		return this.sequence == null;
	}
	
	@Override
	public void acceptElement(int i) {
		if( !this.isEmpty() && this.nextValues.getX() == i) {
			switch(this.sequence) {
			case POWER2:
				this.next(this.nextValues.getY() * 2);
				break;
			case FLIP:
				this.next(i);
				break;
			case RAMBLE:
				if(this.nextValues.getX() == 0) {
					this.next(this.nextValues.getY() + 1);
				} else {
					this.next(0);
				}
				break;
			case FIBONACCI:
				this.next(this.nextValues.getX() + this.nextValues.getY());
				break;
			}
			
		}else {
			throw new IllegalStateException();
		}
	}
	
	private void next(int nextValue) {
		switch(this.sequence) {
		case POWER2:
		case FLIP:
		case FIBONACCI:
			this.nextValues.setX(this.nextValues.getY());
			this.nextValues.setY(nextValue);
			break;
		case RAMBLE:
			if(nextValue == 0) {
				this.nextValues.setX(0);
			} else {
				this.nextValues.setX(this.nextValues.getY());
				this.nextValues.setY(nextValue);
			}
			break;
		}
	}
	
	
}
