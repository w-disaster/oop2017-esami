package a03a.e1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ExamsManagementImpl implements ExamsManagement {
	
	private Map<Exam, List<Pair<Student, Optional<Integer>>>> exams;
	private List<Student> students;
	
	public ExamsManagementImpl() {
		this.exams = new HashMap<>();
		this.students = new ArrayList<>();
	}
	
	private Optional<Exam> getCurrentExam() {
		return this.exams.keySet().stream()
				.filter(e -> e.getY().equals(true))
				.findAny();
	}
	
	private Exam getExamByName(String examName) {
		return this.exams.keySet().stream()
				.filter(e -> e.getX().getY().equals(examName))
				.findAny()
				.get();
	}
	
	private Student getStudentById(int studentId) {
		return this.students.stream()
				.filter(s -> s.getX().equals(studentId))
				.findAny()
				.get();
	}

	@Override
	public void createStudent(int studentId, String name) {
		this.students.add(new Student(studentId, name));
	}

	@Override
	public void createExam(String examName, int incrementalId) {
		this.exams.put(new Exam(incrementalId, examName, false), new ArrayList<>());
	}

	@Override
	public void registerStudent(String examName, int studentId) {
		this.exams.get(this.getExamByName(examName)).add(new Pair<>(this.getStudentById(studentId), Optional.empty()));
	}

	@Override
	public void examStarted(String examName) {
		if(this.getCurrentExam().isPresent()) {
			throw new IllegalStateException();
		}
		Exam exam = this.getExamByName(examName);
		List<Pair<Student, Optional<Integer>>> students = this.exams.get(exam);
		this.exams.remove(exam, students);
		this.exams.put(new Exam(exam.getX().getX(), exam.getX().getY(), true), students);
	}

	@Override
	public void registerEvaluation(int studentId, int evaluation) {
		Student student = this.getStudentById(studentId);
		Exam exam = this.getCurrentExam().get();
		Integer index = this.exams.get(this.getCurrentExam().get())
				.indexOf(new Pair<>(student, Optional.empty()));
		this.exams.get(this.getCurrentExam().get()).set(index, new Pair<>(student, Optional.of(evaluation)));
	}

	@Override
	public void examFinished() {
		if(this.getCurrentExam().isEmpty()) {
			throw new IllegalStateException();
		}
		Exam currentExam = this.getCurrentExam().get();
		List<Pair<Student, Optional<Integer>>> students = this.exams.get(currentExam);
		this.exams.remove(currentExam, students);
		this.exams.put(new Exam(currentExam.getX().getX(), currentExam.getX().getY(), false), students);
	}

	@Override
	public Set<Integer> examList(String examName) {
		return this.exams.get(this.getExamByName(examName)).stream()
				.map(s -> s.getX().getX())
				.collect(Collectors.toSet());
	}

	@Override
	public Optional<Integer> lastEvaluation(int studentId) {
		return this.exams.entrySet().stream()
				.filter(e -> e.getValue().stream()
						.map(s -> s.getX())
						.collect(Collectors.toList())
						.contains(this.getStudentById(studentId)))
				.map(e -> new Pair<Integer, Optional<Integer>>(e.getKey().getX().getX(), 
						e.getValue().stream()
						.filter(s -> s.getX().equals(this.getStudentById(studentId)))
						.map(s -> s.getY())
						.findFirst()
						.get()))
				.max((e1, e2) -> e1.getX() - e2.getX())
				.get().getY();
	}

	@Override
	public Map<String, Integer> examStudentToEvaluation(String examName) {
		return this.exams.get(this.getExamByName(examName))
				.stream()
				.filter(s -> s.getY().isPresent())
				.map(s -> Map.entry(s.getX().getY(), s.getY().get()))
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
	}

	@Override
	public Map<Integer, Integer> examEvaluationToCount(String examName) {
		return this.exams.get(this.getExamByName(examName))
				.stream()
				.filter(s -> s.getY().isPresent())
				.filter(s -> s.getY().get() > 0)
				.map(s -> Map.entry(s.getX().getX(), s.getY().get()))
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
	}

}
