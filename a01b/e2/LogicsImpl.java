package a01b.e2;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class LogicsImpl implements Logics{

	public static final Integer MAX_VALUE = 99;
	private Map<Integer, Integer> randomValues;
	private Integer minValue = MAX_VALUE;
	
	public LogicsImpl(int size) {
		this.randomValues = new HashMap<>();
		Random rnd = new Random();
		Integer next;
		for(Integer i = 0; i < size; i++) {
			next = rnd.nextInt(MAX_VALUE);
			if(next < this.minValue) {
				this.minValue = next;
			}
			this.randomValues.put(i, next);
		}
	}

	@Override
	public boolean hit(Integer key) {
		if(this.randomValues.get(key).equals(this.minValue)) {
			this.remove(key);
			return true;
		}
		return false;
	}

	@Override
	public void remove(Integer key) {
		this.randomValues.remove(key);
		this.generateMinimum();
	}

	@Override
	public void generateMinimum() {
		this.minValue = MAX_VALUE;
		this.randomValues.values().forEach(value -> {
			if(value < this.minValue) {
				this.minValue = value;
			}
		});
	}

	
	
}
