package a01b.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.*;

import javax.swing.*;

public class GUI extends JFrame{
	
	private int timesClicked;
	
	public GUI(int size){
		this.timesClicked = 0;
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		Logics logics = new LogicsImpl(size);
		Map<Integer, JButton> bmap = new HashMap<>();
		JPanel jp = new JPanel();
		
		for(int i = 0; i < size; i++) {
			bmap.put(i, new JButton("*"));
		}
		
		bmap.entrySet().forEach(e -> {
			jp.add(e.getValue());
			e.getValue().addActionListener(a -> {
				this.timesClicked ++;
				System.out.println(this.timesClicked);
				e.getValue().setEnabled(!logics.hit(e.getKey()));
			});
		});
		
		this.add(jp);
		
		this.setVisible(true);
	}
	
}
