package a01b.e2;

public interface Logics {

	/**
	 * @param key
	 * @return true if the key's value if the minimum 
	 */
	boolean hit(Integer key);
	
	/**
	 * @param key
	 * removes an entry with key : key
	 */
	void remove(Integer key);
	
	/**
	 * Generates the minimum
	 */
	void generateMinimum();
}
