package a01b.e1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class TraceImpl<X> implements Trace<X>{

	private Iterator<Event<X>> itrace;
	
	public TraceImpl(Iterator<Event<X>> itrace) {
		super();
		this.itrace = itrace;
	}
	
	@Override
	public Stream<Event<X>> toStream(){
		List<Event<X>> elist = new ArrayList<>();
		Iterator<Event<X>> tit = this.iterator();
		while(tit.hasNext()) {
			elist.add(tit.next());
		}
		return(elist.stream());
	}
	
	@Override
	public Optional<Event<X>> nextEvent() {
		return Optional.ofNullable(itrace.hasNext() ? itrace.next() : null);
	}

	@Override
	public Iterator<Event<X>> iterator() {
		return this.itrace;
	}

	@Override
	public void skipAfter(int time) {
		while(itrace.hasNext()) {
			if (itrace.next().getTime() == time){
				return;
			}
		}
	}

	@Override
	public Trace<X> combineWith(Trace<X> trace) {
		return new TraceImpl<>(Stream.concat(this.toStream(), trace.toStream())
				.sorted((e1, e2) -> e1.getTime() - e2.getTime())
				.iterator());
	}

	@Override
	public Trace<X> dropValues(X value) {
		return new TraceImpl<>(this.toStream()
				.filter(e -> !e.getValue().equals(value))
				.iterator());
	}
}
