package a01b.e1;

import java.util.function.Supplier;
import java.util.stream.IntStream;

public class TraceFactoryImpl implements TraceFactory{

	@Override
	public <X> Trace<X> fromSuppliers(Supplier<Integer> sdeltaTime, Supplier<X> svalue, int size) {
		return new TraceImpl<X>(IntStream.iterate(0, i -> i + sdeltaTime.get())
				.<Event<X>>mapToObj(t -> new EventImpl<X>(t, svalue.get()))
				.limit(size)
				.iterator());
	}

	@Override
	public <X> Trace<X> constant(Supplier<Integer> sdeltaTime, X value, int size) {
		return new TraceImpl<X>(IntStream.iterate(0, i -> i + sdeltaTime.get())
				.<Event<X>>mapToObj(t -> new EventImpl<X>(t, value))
				.limit(size)
				.iterator());
	}

	@Override
	public <X> Trace<X> discrete(Supplier<X> svalue, int size) {
		return new TraceImpl<X>(IntStream.iterate(0, i -> i + 1)
				.<Event<X>>mapToObj(t -> new EventImpl<X>(t, svalue.get()))
				.limit(size)
				.iterator());
	}

}
